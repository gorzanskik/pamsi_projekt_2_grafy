/*****************************
	   Algorytm Dijkstry
		  bazujacy na
	  macierzy sasiedztwa
			   i
	  li�cie s�siedztwa
*****************************/

#include <stdio.h> 
#include <limits.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <chrono>
#include <sstream>
#include <time.h>
#include <math.h>
#include <malloc.h>

typedef std::chrono::high_resolution_clock Clock;

using namespace std;

// liczba wierzcholkow - przyk�adowo
#define V 1000
const int MAX_N = 1000; // 1000;
const int INF = 2147483647;

struct Wezel
{
	int numer;
	int waga;
	struct Wezel* nastepny;
};

void sleep(int ms) {
	clock_t target = clock() + ms;
	while (clock() < target);
}

void stworz_plik(int a, int b) {
	int plusplus = 0;
	ostringstream ss;
	ss << "Wyp_gr_" << a << "_" << b << ".txt";
	string str = ss.str();

	string const nazwaPliku(str);
	ofstream mojStrumien(nazwaPliku.c_str());

	if (mojStrumien) {

		int q;
		int p;
		srand((unsigned)time(NULL));



		//Generuj� plik z danymi

		mojStrumien << "1" << endl;
		mojStrumien << a * a << endl;
		for (int k = 1; k <= a; k++) {
			for (int o = 1; o <= a; o++) {
				q = rand() % 100;
				p = rand() % 100;
				if (p <= b) {
					mojStrumien << k << " " << o << " " << q << endl;
					plusplus++;
				}
			}
		}
		cout << "Ilosc lini: " << plusplus << endl;
	}
	else {
		cout << "B��D: nie mo�na pisa� do pliku" << endl;
	}
}

//algorytm na li�cie

void Dijkstry(string nazwa_pliku) {

	int i, j, u, n, il_krawedz, x, y, waga_kraw;
	int w_startowy = 0;
	/*
	int d[MAX_N + 1], p[MAX_N + 1];
	bool QS[MAX_N + 1];
	*/
	int* d, * p;
	bool* QS;
	d = (int*)malloc(MAX_N + 1);
	p = (int*)malloc(MAX_N + 1);
	QS = (bool*)malloc(MAX_N + 1);

	if (d != NULL && p != NULL && QS != NULL) {

		struct Wezel* Lista_sasiedztwa[MAX_N + 1], * pw;

		for (i = 1; i <= MAX_N; i++) {
			d[i] = INF;
			p[i] = 0;
			QS[i] = false;
			Lista_sasiedztwa[i] = NULL;
		}
		n = 0;

		/***********************************/

		ifstream plik(nazwa_pliku);
		int dana;

		if (plik.good() == true) {
			while (!plik.eof()) {
				plik >> w_startowy;
				plik >> il_krawedz;



				/***********************************/

				//cin >> w_startowy;
				//cin >> il_krawedz;

				for (i = 1; i <= il_krawedz; i++) {
					plik >> x >> y >> waga_kraw;
					if (x > n) n = x;
					if (y > n) n = y;
					pw = new Wezel;
					pw->nastepny = Lista_sasiedztwa[x]; pw->numer = y; pw->waga = waga_kraw;
					Lista_sasiedztwa[x] = pw;
				}

			}
			plik.close();
		}



		//cout << endl;
		d[w_startowy] = 0;

		for (i = 1; i <= n; i++) {
			u = -1;
			for (j = 1; j <= n; j++)
				if (!QS[j] && ((u == -1) || (d[j] < d[u]))) u = j;

			QS[u] = true;

			pw = Lista_sasiedztwa[u];
			while (pw) {
				if (!QS[pw->numer] && (d[pw->numer] > d[u] + pw->waga)) {
					d[pw->numer] = d[u] + pw->waga;
					p[pw->numer] = u;
				}
				pw = pw->nastepny;
			}
		}

		int stos[MAX_N], ws;

		for (i = 1; i <= n; i++) {
			//cout << i << ": ";
			ws = 0; j = i;

			while (j) {
				stos[ws++] = j; j = p[j];
			}

			while (ws) /*cout << */stos[--ws]/* << " "*/;

			if (d[i] == INF) {
				//cout<<"Brak polaczenia"<<endl;
			}
			else {
				//cout << "#" << d[i] << endl;  
			}
		}

		free(d); free(p); free(QS);
	}
}

//funkcja urzyta w alg dikstry dla macierzy, zaimplementowanym poni�ej

int minDistance(int dist[], bool sptSet[]) {
	// Initializuj� wartosc minimaln�
	int min = INT_MAX, min_index;

	for (int v = 0; v < V; v++)
		if (sptSet[v] == false && dist[v] <= min)
			min = dist[v], min_index = v;

	return min_index;
}

// Funkcja wypisania tablicy odleglosci dla alg dikstry dla macierzy, zaimplementowanym poni�ej
void printSolution(int dist[], int n) {

	cout << "Vertex   Distance from Source\n";
	for (int i = 0; i < V; i++)
		cout << i << " " << dist[i] << endl;
}

// dijkstra za pomoc� macierzy sasiedztwa
void dijkstra(int graph[V][V], int src) {
	int dist[V];     //tablica przechowywujaca najk�trz� �ciezk�

	bool sptSet[V];

	//inicjalizacja
	for (int i = 0; i < V; i++)
		dist[i] = INT_MAX, sptSet[i] = false;

	dist[src] = 0;

	// znajduj� najkr�trz� �cie�k� do wszystkich wierzcho�k�w
	for (int count = 0; count < V - 1; count++)
	{
		// Wybierz minimalny wierzcho�ek odleg�o�ci ze zbioru wierzcho�k�w jeszcze nie przetworzonych. u jest zawsze r�wne src w pierwszej iteracji.
		int u = minDistance(dist, sptSet);

		// Zaznacz wybrany wierzcho�ek jako przetworzony
		sptSet[u] = true;

		// Zaktualizuj warto�� dyst s�siednich wierzcho�k�w pobranego wierzcho�ka.
		for (int v = 0; v < V; v++)

			// Aktualizuj dist [v] tylko wtedy, gdy nie ma go w sptSet, istnieje kraw�d� od u do v, a ca�kowita waga �cie�ki od src do v przez u jest mniejsza ni� bie��ca warto�� dist [v]
			if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX
				&& dist[u] + graph[u][v] < dist[v])
				dist[v] = dist[u] + graph[u][v];
	}

	// wypisz tablic� odleg�o�ci
	//printSolution(dist, V); 
}
// nie zd��y�em tego napisa� �adniej, dlatego wywo�ywa�em t� funkcj� r�cznie wiele razy dla r�zniej ilosci wierzcholk�w 
int main() {
	double czasm25 = 0, czasm50 = 0, czasm75 = 0, czasm100 = 0;
	double czast25 = 0, czast50 = 0, czast75 = 0, czast100 = 0;

	int graph[V][V];

	/////////// graf wype�niony /////////////////////////////////////////////////////////////////////////////////////////

	for (int i = 0; i < 10; ++i) {
		stworz_plik(1000, 100);
		sleep(10000); //aby kolejny stworzony plik r�zni� si� od poprzedniego

		int x, y, waga, il_krawedz, sqrtilkraw, w_startowy;
		int n = 0;
		ifstream plik("Wyp_gr_1000_100.txt");

		int** mojgraf = new int* [MAX_N]; //alokacja pamieci
		for (int i = 1; i <= MAX_N; ++i) {
			mojgraf[i - 1] = new int[MAX_N]; //alokacja pamieci

			for (int j = 1; j <= MAX_N; ++j) //wpisanie wartosci do tablicy
				mojgraf[i - 1][j - 1] = 0;
		}

		if (plik.good() == true) {
			while (!plik.eof()) {
				plik >> w_startowy;
				plik >> il_krawedz;
				sqrtilkraw = (int)(sqrt(il_krawedz));

				for (int i = 1; i <= il_krawedz; i++) {
					plik >> x >> y >> waga;
					if (x > n) n = x;
					if (y > n) n = y;
					mojgraf[x - 1][y - 1] = waga;
				}
			}
			plik.close();
		}

		for (int i = 0; i < V; i++) {
			for (int j = 0; j < V; j++) {
				graph[i][j] = mojgraf[i][j];
			}
		}

		auto start = Clock::now();
		dijkstra(graph, 0);
		auto stop = Clock::now();
		czasm100 += (chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count());
	}



	for (int i = 0; i < 10; ++i) {
		stworz_plik(1000, 100);
		sleep(10000);

		auto start = Clock::now();
		Dijkstry("Wyp_gr_1000_100.txt");
		auto stop = Clock::now();
		czast100 += (chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count());
	}

	//// graf wype�nionmy w 75% //////////////////////////////////////////////////////////////////////////////////////////////// 

	for (int i = 0; i < 10; ++i) {
		stworz_plik(1000, 75);
		sleep(10000);

		int x, y, waga, il_krawedz, sqrtilkraw, w_startowy;
		int n = 0;
		ifstream plik("Wyp_gr_1000_75.txt");

		int** mojgraf = new int* [MAX_N]; //alokacja pamieci
		for (int i = 1; i <= MAX_N; ++i) {
			mojgraf[i - 1] = new int[MAX_N]; //alokacja pamieci

			for (int j = 1; j <= MAX_N; ++j) //wpisanie wartosci do tablicy
				mojgraf[i - 1][j - 1] = 0;
		}

		if (plik.good() == true) {
			while (!plik.eof()) {
				plik >> w_startowy;
				plik >> il_krawedz;
				sqrtilkraw = (int)(sqrt(il_krawedz));

				//cout<<w_startowy<<endl<<il_krawedz<<endl;

				for (int i = 1; i <= il_krawedz; i++) {
					plik >> x >> y >> waga;
					//cout<<"x= "<<x<<endl<<"y= "<<y<<endl<<"waga= "<<waga<<endl;
					if (x > n) n = x;
					if (y > n) n = y;
					mojgraf[x - 1][y - 1] = waga;
				}
			}
			plik.close();
		}

		for (int i = 0; i < V; i++) {
			for (int j = 0; j < V; j++) {
				graph[i][j] = mojgraf[i][j];
			}
		}

		auto start = Clock::now();
		dijkstra(graph, 0);
		auto stop = Clock::now();
		czasm75 += (chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count());
	}



	for (int i = 0; i < 10; ++i) {
		stworz_plik(1000, 75);
		sleep(10000);

		auto start = Clock::now();
		Dijkstry("Wyp_gr_1000_75.txt");
		auto stop = Clock::now();
		czast75 += (chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count());
	}

	//// graf wype�nionmy w 50% ////////////////////////////////////////////////////////////////////////////////////////////////

	for (int i = 0; i < 10; ++i) {
		stworz_plik(1000, 50);
		sleep(10000);

		int x, y, waga, il_krawedz, sqrtilkraw, w_startowy;
		int n = 0;
		ifstream plik("Wyp_gr_1000_50.txt");

		int** mojgraf = new int* [MAX_N]; //alokacja pamieci
		for (int i = 1; i <= MAX_N; ++i) {
			mojgraf[i - 1] = new int[MAX_N]; //alokacja pamieci

			for (int j = 1; j <= MAX_N; ++j) //wpisanie wartosci do tablicy
				mojgraf[i - 1][j - 1] = 0;
		}

		if (plik.good() == true) {
			while (!plik.eof()) {
				plik >> w_startowy;
				plik >> il_krawedz;
				sqrtilkraw = (int)(sqrt(il_krawedz));

				//cout<<w_startowy<<endl<<il_krawedz<<endl;

				for (int i = 1; i <= il_krawedz; i++) {
					plik >> x >> y >> waga;
					//cout<<"x= "<<x<<endl<<"y= "<<y<<endl<<"waga= "<<waga<<endl;
					if (x > n) n = x;
					if (y > n) n = y;
					mojgraf[x - 1][y - 1] = waga;
				}
			}
			plik.close();
		}

		for (int i = 0; i < V; i++) {
			for (int j = 0; j < V; j++) {
				graph[i][j] = mojgraf[i][j];
			}
		}

		auto start = Clock::now();
		dijkstra(graph, 0);
		auto stop = Clock::now();
		czasm50 += (chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count());
	}



	for (int i = 0; i < 10; ++i) {
		stworz_plik(1000, 50);
		sleep(10000);

		auto start = Clock::now();
		Dijkstry("Wyp_gr_1000_50.txt");
		auto stop = Clock::now();
		czast50 += (chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count());
	}

	//// graf wype�nionmy w 25% ////////////////////////////////////////////////////////////////////////////////////////////////

	for (int i = 0; i < 10; ++i) {
		stworz_plik(1000, 25);
		sleep(10000);

		int x, y, waga, il_krawedz, sqrtilkraw, w_startowy;
		int n = 0;
		ifstream plik("Wyp_gr_1000_25.txt");

		int** mojgraf = new int* [MAX_N]; //alokacja pamieci
		for (int i = 1; i <= MAX_N; ++i) {
			mojgraf[i - 1] = new int[MAX_N]; //alokacja pamieci

			for (int j = 1; j <= MAX_N; ++j) //wpisanie wartosci do tablicy
				mojgraf[i - 1][j - 1] = 0;
		}

		if (plik.good() == true) {
			while (!plik.eof()) {
				plik >> w_startowy;
				plik >> il_krawedz;
				sqrtilkraw = (int)(sqrt(il_krawedz));

				//cout<<w_startowy<<endl<<il_krawedz<<endl;

				for (int i = 1; i <= il_krawedz; i++) {
					plik >> x >> y >> waga;
					//cout<<"x= "<<x<<endl<<"y= "<<y<<endl<<"waga= "<<waga<<endl;
					if (x > n) n = x;
					if (y > n) n = y;
					mojgraf[x - 1][y - 1] = waga;
				}
			}
			plik.close();
		}

		for (int i = 0; i < V; i++) {
			for (int j = 0; j < V; j++) {
				graph[i][j] = mojgraf[i][j];
			}
		}

		auto start = Clock::now();
		dijkstra(graph, 0);
		auto stop = Clock::now();
		czasm25 += (chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count());
	}



	for (int i = 0; i < 10; ++i) {
		stworz_plik(1000, 25);
		sleep(10000);

		auto start = Clock::now();
		Dijkstry("Wyp_gr_1000_25.txt");
		auto stop = Clock::now();
		czast25 += (chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count());
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////


	//wypisanie wynikow
	cout << "dikstra 1000: " << endl << "macierz" << endl;
	cout <<"Algorytm Dijkstry macierzy sasiedztwa 100%: " << czasm100 / (10000000) << " ms" << endl;
	cout <<"Algorytm Dijkstry macierzy sasiedztwa 75%:  " << czasm75 / (10000000) << " ms" << endl;
	cout <<"Algorytm Dijkstry macierzy sasiedztwa 50%:  " << czasm50 / (10000000) << " ms" << endl;
	cout <<"Algorytm Dijkstry macierzy sasiedztwa 25%:  " << czasm25 / (10000000) << " ms" << endl << "lista" << endl;
	cout <<"Algorytm Dijkstry listy sasiedztwa 100%:    " << czast100 / (10000000) << " ms" << endl;
	cout <<"Algorytm Dijkstry listy sasiedztwa 75%:     " << czast75 / (10000000) << " ms" << endl;
	cout <<"Algorytm Dijkstry listy sasiedztwa 50%:     " << czast50 / (10000000) << " ms" << endl;
	cout <<"Algorytm Dijkstry listy sasiedztwa 25%:     " << czast25 / (10000000) << " ms" << endl;


	return 0;
}