#include<cstdio>
#include<vector>
#include<cmath>
#include <cstdlib>
#include <iomanip>
#include <stdio.h> 
#include <limits.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <chrono>
#include <sstream>
#include <time.h>
#include <math.h>

using namespace std;

typedef std::chrono::high_resolution_clock Clock;

int n, m, s;                //n - liczba wierzcho�k�w, m - liczba kraw�dzi, s - punkt, z kt�rego zaczynamy

vector<vector<int>> E;
vector<int>D;

const int INF = (1 << 30);  //Jest to warto�� na tyle du�a, �e dla liczb z zakresu int powinna pos�u�y� jako niesko�czono��

void sleep(int ms) {
	clock_t target = clock() + ms;
	while (clock() < target);
}

void stworz_plik(int a, int b) {
	int plusplus = 0;
	ostringstream ss;
	ss << "Wyp_gr_" << a << "_" << b << ".txt";
	string str = ss.str();

	string const nazwaPliku(str);
	ofstream mojStrumien(nazwaPliku.c_str());

	if (mojStrumien) {

		int q;
		int p;
		srand((unsigned)time(NULL));

		//Generuj� plik z danymi

		mojStrumien << "1" << endl;
		mojStrumien << a * a << endl;
		for (int k = 1; k <= a; k++) {
			for (int o = 1; o <= a; o++) {
				
				q = rand() % 100;
				p = rand() % 100;
				if (p <= b) {
					mojStrumien << k << " " << o << " " << q << endl;
					plusplus++;
				}
			}
		}
		cout << "Ilosc lini: " << plusplus << endl;
	}
	else {
		cout << "B��D: nie mo�na pisa� do pliku" << endl;
	}
}


int main() {

	int czasm100 = 0;

	for (int i = 0; i < 5; i++) {
		stworz_plik(100, 50);
		sleep(1000000);
		ifstream plik("Wyp_gr_100_50.txt");

		if (plik.good() == true) {
			while (!plik.eof()) {
				plik >> s;
				s = s - 1;
				plik >> m;
				n = (int)(sqrt(m));

				E.resize(m);

				for (int i = 0; i < m; i++) {
					int u, v, waga;
					plik >> u >> v >> waga;
					E[i].resize(3);
					E[i][0] = u - 1;
					E[i][1] = v - 1;
					E[i][2] = waga;
				}
			}
			plik.close();
		}

		D.resize(n);

		for (int i = 0; i < n; i++) D[i] = INF;   //D - tablica koszt�w drog ze wszystkich wierzcho�kow
		D[s] = 0;                               //wierzch startowy

		auto start = Clock::now();              //zaczynam mierzy� algorytmowi czas

		for (int i = 1; i <= n; i++)
		{
			for (int j = 0; j < m; j++)         // sprawdzamy, czy istnieje kraw�d�, ale dla kt�rej nie zachodzi Obserwacja 1.
			{
				int u = E[j][0], v = E[j][1], waga = E[j][2];
				if (D[u] != INF && D[u] + waga < D[v])
				{
					D[v] = D[u] + waga;

					if (i == n)                   // je�eli i dojdzie do n  to:
					{
						cout << "Znaleziono cykl o ujemnej wadze" << endl;
						return 0;
					}
				}
			}
		}
		auto stop = Clock::now(); //ko�cz� mierzy� czas
		czasm100 += (int)((chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count()));
		//cout<<czasm100/(10000)<<endl;

	}
	cout << "�redni czas wykonania algorytmu (100 x 100, 50): " << czasm100 / (10000 * 5) << endl;

	////wypisywanie wyniku na ekran
	for (int i = 0; i < n; i++){

		if (i != s && D[i] < INF) 
			
			cout << i << " " << D[i] << endl;

	}

	return 0;
}
