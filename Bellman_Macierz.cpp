#include <cstdlib>
#include <iomanip>
#include <stdio.h> 
#include <limits.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <chrono>
#include <sstream>
#include <time.h>
#include <math.h>

using namespace std;
#define MAX 99999
#define V 200

const int MAX_N = 200;
const int INF = 2147483647;

typedef std::chrono::high_resolution_clock Clock;

void BellmanFord(int, int, int**);
void wyswietl(int, int**);
void wyswietl_BellmanFord(int*, int*, int, int);


void sleep(int ms) {
	clock_t target = clock() + ms;
	while (clock() < target);
}

void stworz_plik(int a, int b) {
	int plusplus = 0;
	ostringstream ss;
	ss << "Wyp_gr_" << a << "_" << b << ".txt";
	string str = ss.str();

	string const nazwaPliku(str);
	ofstream mojStrumien(nazwaPliku.c_str());

	if (mojStrumien) {

		int q;
		int p;
		srand((unsigned)time(NULL));

		//Generuj� plik z danymi

		mojStrumien << "1" << endl;
		mojStrumien << a * a << endl;
		for (int k = 1; k <= a; k++) {
			for (int o = 1; o <= a; o++) {


				q = rand() % 100;
				p = rand() % 100;
				if (p <= b) {
					mojStrumien << k << " " << o << " " << q << endl;
					plusplus++;
				}
			}
		}
		cout << "Ilosc lini: " << plusplus << endl;
	}
	else {
		cout << "B��D: nie mo�na pisa� do pliku" << endl;
	}
}

void wyswietl_BellmanFord(int* koszt, int* numer, int l_wierzch, int wierzch_start)
{
	int* S = new int[l_wierzch];          // tymczasowy stos
	int sptr = 0;
	cout << "wyniki dla start: " << wierzch_start << endl;
	cout << "End: " << " Dist: " << "Path: " << endl;
	for (int i = 0; i < l_wierzch; i++)
	{
		cout.width(3);
		cout << i << " |";
		cout.width(3);
		cout << koszt[i] << "|   ";

		for (int x = i; x != -1; x = numer[x])   // umieszcz. wierzcholki na stosie
		{
			S[sptr++] = x;                      // w kolejno�ci od ostatniego do pierwszego

			while (sptr)                           // drukujemy w kolejnosci od konca odl
				cout << S[--sptr] << " ";

		}
		cout << endl;

	}
}

void BellmanFord(int wierzch_start, int l_wierzch, int** macierz)
{
	int* koszt = new int[l_wierzch];       //koszt dojscia
	int* numer = new int[l_wierzch];    //i-ty numer wierzcholka grafu
	for (int i = 0; i < l_wierzch; i++)          //ktory jest poprzednikiem wierz. i-tego na 
	{                                       //na najkrotszej sciezce        
		numer[i] = -1;      //p             //domyslny numer
		koszt[i] = MAX;     //d             //dimyslny koszt dotarcia (nieskonczonosc)
	}
	koszt[wierzch_start] = 0;                 //koszt dotarcia z wierzcholka startowego do startowego

	//teraz petla glowna
	for (int i = 1; i <= l_wierzch; i++)
	{
		for (int w = 0; w < l_wierzch; w++)
		{
			for (int k = 0; k < l_wierzch; k++)
			{

				if ((macierz[w][k] != 0) && (koszt[k]) >= (koszt[w] + macierz[w][k]))
				{
					koszt[k] = koszt[w] + macierz[w][k];
					numer[k] = w;

				}

			}
		}

	}
	//wyswietl_BellmanFord(koszt, numer, l_wierzch, wierzch_start);
}

void wyswietl(int l_wierzch, int** macierz) {

	for (int j = 0; j < l_wierzch; j++) {
		cout << endl;
		for (int k = 0; k < l_wierzch; k++)
			cout << setw(3) << macierz[j][k];
	}
}

int main() {

	int czasm200 = 0;
	//int graph[V][V];
	int x, y, waga, il_krawedz, sqrtilkraw, w_startowy; // n;
	int wierzch_start = 0;
	int n = 0;


	int** mojgraf = new int* [MAX_N];         //alokacja pamieci
	for (int i = 1; i <= MAX_N; ++i) {
		mojgraf[i - 1] = new int[MAX_N];   //alokacja pamieci

		for (int j = 1; j <= MAX_N; ++j) //wpisanie wartosci do tablicy
			mojgraf[i - 1][j - 1] = 0;
	}

	for (int i = 0; i < 10; i++) {

		stworz_plik(200, 100);
		sleep(1000000);                          //aby kolejny stworzony plik r�zni� si� od poprzedniego

		ifstream plik("Wyp_gr_200_100.txt");

		if (plik.good() == true) {
			while (!plik.eof()) {
				plik >> w_startowy;
				plik >> il_krawedz;
				sqrtilkraw = (int)(sqrt(il_krawedz));

				for (int i = 1; i <= il_krawedz; i++) {
					plik >> x >> y >> waga;
					if (x > n) n = x;
					if (y > n) n = y;
					mojgraf[x - 1][y - 1] = waga;
				}
			}
			plik.close();
		}

		int l_wierzch = V;

		//int wierzch_start =0;
		int** macierz;
		macierz = new int* [l_wierzch];
		for (int i = 0; i < l_wierzch; i++)
			macierz[i] = new int[l_wierzch];


		for (int i = 0; i < V; i++) {
			for (int j = 0; j < V; j++) {
				macierz[i][j] = mojgraf[i][j];
			}
		}


		//wyswietl(l_wierzch, macierz);
		//cout << endl;
		//cout<<"zaczynam algorytm"<<endl;
		auto start = Clock::now();
		BellmanFord(wierzch_start, l_wierzch, macierz);
		auto stop = Clock::now();
		//cout<<"ko�cz� algorytm"<<endl;
		czasm200 += (int)((chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count()));
	}
	cout << "Algorytm Bellmana-Forda macierzy sasiedztwa 100%: " << czasm200 / (10000 * 10) << " ms" << endl;

	return 0;
}